package com.galvanize;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class ShoppingBasketTest {
    @Test
    @DisplayName("Can we create an empty shopping basket?")
    public void emptyShoppingBasket() {
        ShoppingBasket basket = new ShoppingBasket(new ArrayList<>());
        assertEquals(0, basket.getTotal(), 0);
    }

    @Test
    @DisplayName("Can we add one item to the basket?")
    public void totalOfASingleItem() {
        ShoppingBasket basket = new ShoppingBasket(Arrays.asList(new Item(100, 1)));
        assertEquals(100, basket.getTotal(), 0);

    }

    @Test
    @DisplayName("Can we add two items to the basket?")
    public void totalOfTwoItems() {
        ShoppingBasket basket = new ShoppingBasket(Arrays.asList(new Item(100, 2)));
        assertEquals(100, basket.getTotal(), 0);
    }
}
